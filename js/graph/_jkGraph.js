$(document).ready(function () {
  var url = window.location.href.split('/')[0] + '/';
  var ctx = document.getElementById("jkGraph").getContext('2d');
  var jkChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [0],
        backgroundColor: [
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 99, 132, 0.2)',
        ],
        borderColor: [
          'rgba(54, 162, 235, 1)',
          'rgba(255,99,132,1)',
        ],
        hoverBackgroundColor: [
          'rgba(54, 162, 235, 1)',
          'rgba(255,99,132,1)',
        ],
        borderWidth: 1
      }],

      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: ['Total'],
    },
  });

  $.ajax({
    url: url + 'backend/graph/_jkGraph.php',
    type: 'GET',
    dataType: 'JSON',
    success: function (data) {
      console.log(data.pria);
      var mapped = Object.keys(data).map(function (uid) {
        return parseInt((data[uid].uid = uid) && data[uid]);
      });

      jkChart.data.datasets.map(function (o) {
        o.data = mapped;
        return o;
      })
      jkChart.data.labels = ["Pria", "Wanita"];
      jkChart.update();
      $('#jumlah_laki_laki').html(data.pria);
      $('#jumlah_perempuan').html(data.wanita);

      let countPria = parseInt(data.pria)
      let countWanita = parseInt(data.wanita)
      
      let persenPria = countPria / data.allData * 100;
      let persenWanita = countWanita / data.allData * 100;

      $('#jumlah_persentase_laki_laki').html(persenPria+'%');
      $('#jumlah_persentase_perempuan').html(persenWanita+'%');
    }
  });
  setInterval(function () {
    $.ajax({
      url: url + 'backend/graph/_jkGraph.php',
      type: 'GET',
      dataType: 'JSON',
      success: function (data) {
        
        var mapped = Object.keys(data).map(function (uid) {
          return parseInt((data[uid].uid = uid) && data[uid]);
        });

        jkChart.data.datasets.map(function (o) {
          o.data = mapped;
          return o;
        })
        jkChart.data.labels = ["Pria", "Wanita"];
        jkChart.update();
        $('#jumlah_laki_laki').html(data.pria);
        $('#jumlah_perempuan').html(data.wanita);

        let countPria = parseInt(data.pria)
        let countWanita = parseInt(data.wanita)
        let allData = countPria + countWanita;

        let persenPria = countPria / allData * 100;
        let persenWanita = countWanita / allData * 100;

        $('#jumlah_persentase_laki_laki').html(persenPria+'%');
        $('#jumlah_persentase_perempuan').html(persenWanita+'%');
        }
    });

  }, 1000);
});
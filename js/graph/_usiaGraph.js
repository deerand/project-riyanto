$(document).ready(function () {
  var url = window.location.href.split('/')[0] + '/';
  var usiaGraph = document.getElementById("usiaGraph");

  $.ajax({
    url: url + 'backend/graph/_usiaGraph.php',
    type: 'GET',
    dataType: 'JSON',
    success: function (data) {
      let getKey = Object.keys(data);
      let getInt = Object.keys(data).length;
      var mapped = Object.keys(data).map(function (uid) {
        return parseInt((data[uid].uid = uid) && data[uid]);
      });      
      usiaChart.data.datasets.map(function (o) {
        o.data = mapped;
        return o;
      })
      usiaChart.data.labels = Object.keys(data);      
      usiaChart.update();
      var total = 0;
      for (var i = 0; i < mapped.length; i++) {
        total += mapped[i] << 0;
      }
      var y = 0;
      for(i= 0;i < getInt; i++)
      {
        y++;
        $('#persentaseUsia').append("<tr><td>"+y+"</td><td>"+getKey[i]+"</td><td>"+mapped[i]+"</td><td>"+mapped[i]/total*100+"%</td></tr>")
      }
    }
  });  
  var usiaChart = new Chart(usiaGraph, {
    type: 'bar',
    beginAtZero: true,
    data: {
      datasets: [{
        label: "Grafik Penduduk Berdasarkan Usia",
        data: [0],
        backgroundColor: [
          "rgba(247, 4, 4, 0.2)",
          "rgba(247, 77, 4, 0.2)",
          "rgba(247, 197, 4, 0.2)",
          "rgba(215, 247, 14, 0.2)",
          "rgba(126, 247, 4, 0.2)",
          "rgba(37, 247, 4, 0.2)",
          "rgba(4, 247, 101, 0.2)",
          "rgba(4, 247, 158, 0.2)",
          "rgba(4, 247, 210, 0.2)",
          "rgba(4, 186, 247, 0.2)",
          "rgba(4, 109, 247, 0.2)",
          "rgba(97, 4, 247, 0.2)",
          "rgba(154, 4, 247, 0.2)"
        ],
        borderColor: [
          "rgb(247, 4, 4)",
          "rgb(247, 77, 4)",
          "rgb(247, 197, 4)",
          "rgb(215, 247, 14)",
          "rgb(126, 247, 4)",
          "rgb(37, 247, 4)",
          "rgb(4, 247, 101)",
          "rgb(4, 247, 158)",
          "rgb(4, 247, 210)",
          "rgb(4, 186, 247)",
          "rgb(4, 109, 247)",
          "rgb(97, 4, 247)",
          "rgb(154, 4, 247)"
        ],
        borderWidth: 1,
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function (value) {
              if (Number.isInteger(value)) {
                return value;
              }
            },
            stepSize: 100
          }
        }]
      }

    }
  });
});
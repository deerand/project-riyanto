$(document).ready(function () {
  var url = window.location.href.split('/')[0] + '/';
  var agamaGraph = document.getElementById("agamaGraph").getContext('2d');

  $.ajax({
    url: url + 'backend/graph/_agamaGraph.php',
    type: 'GET',
    dataType: 'JSON',
    success: function (data) {
      let getKey = Object.keys(data);
      let getInt = Object.keys(data).length;

      var mapped = Object.keys(data).map(function (uid) {
        return parseInt((data[uid].uid = uid) && data[uid]);
      });

      agamaChart.data.datasets.map(function (o) {
        o.data = mapped;
        return o;
      })
      agamaChart.data.labels = ["Islam", "Kristen", "Katolik", "Hindu", "Budha", "Khonghucu", "Kepercayaan"];
      agamaChart.update();
      var total = 0;
      for (var i = 0; i < mapped.length; i++) {
        total += mapped[i] << 0;
      }
      var y = 0;      
      for(i= 0;i < getInt; i++)
      {
        y++;
        $('#persentaseAgama').append("<tr><td>"+y+"</td><td>"+getKey[i]+"</td><td>"+mapped[i]+"</td><td>"+mapped[i]/total*100+"%</td></tr>")
      }

    }
  });

  var agamaChart = new Chart(agamaGraph, {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [0],
        backgroundColor: [
          'rgba(255, 0, 0, 0.5)',
          'rgba(0, 255, 0, 0.5)',
          'rgba(0, 0, 255, 0.5)',
          'rgba(255, 255, 0, 0.5)',
          'rgba(0, 255, 255, 0.5)',
          'rgba(255, 0, 255, 0.5)',
          'rgba(255, 132, 10, 0.5)',
        ],
        borderColor: [
          'rgba(255, 0, 0, 1)',
          'rgba(0, 255, 0, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(255, 255, 0, 1)',
          'rgba(0, 255, 255, 1)',
          'rgba(255, 0, 255, 1)',
          'rgba(255, 132, 10, 1)',
        ],
        hoverBackgroundColor: [
          'rgba(255, 0, 0, 1)',
          'rgba(0, 255, 0, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(255, 255, 0, 1)',
          'rgba(0, 255, 255, 1)',
          'rgba(255, 0, 255, 1)',
          'rgba(255, 132, 10, 1)',
        ],
        borderWidth: 1
      }],

      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: ['Total'],

    },

  });
});
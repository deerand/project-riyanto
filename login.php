<?php include_once "./utils/reverse.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Login Masuk ANDECA</title>

  <?php include_once "./resources/src/_css.php"; ?>

</head>

<body class="login-page">
  <div class="login-box">
    <div class="logo">
      <a href="javascript:void(0);">Login Masuk <b>ANDECA</b></a>
      <!-- <small>Admin BootStrap Based - Material Design</small> -->
    </div>
    <div class="card">
      <div class="body">
        <form id="sign_in" method="POST" action="./backend/login/login.php"> 
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">person</i>
            </span>
            <div class="form-line">
              <input type="text" class="form-control" name="nip" placeholder="NIP" required autofocus>
            </div>
          </div>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
              <input type="password" class="form-control" name="password" placeholder="Password" required>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-4 align-right">
              <input class="btn btn-block bg-pink waves-effect" type="submit" name="login" value="Masuk">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php include_once "./resources/src/_js.php"; ?>
</body>

</html>
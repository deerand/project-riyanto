﻿<?php include_once "./utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
    <?php include_once "./resources/partrials/_header.php"; ?>
    <?php include_once "./resources/src/_css.php"; ?>
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <?php include_once "./resources/partrials/_loader.php"; ?>
    <!-- #END# Page Loader -->
    <!-- Top Bar -->
    <?php include "./resources/partrials/_topbar.php"; ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <?php include_once "./resources/partrials/_userinfo.php"; ?>
            <!-- #User Info -->
            <!-- Menu -->
            <?php include_once "./resources/partrials/_sidebar.php"; ?>
            <!-- #Menu -->
            <!-- Footer -->
            <?php include_once "./resources/partrials/_footer.php"; ?>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">Penduduk</div>
                            <?php
                                include_once "./backend/config/koneksi.php";

                                $querypenduduk = mysqli_query($con,"SELECT * FROM penduduk");
                                $rowcountpenduduk = mysqli_num_rows($querypenduduk);
                            ?>
                            <div class="text number">
                                <?php echo $rowcountpenduduk;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">email</i>
                        </div>
                        <div class="content">
                            <div class="text">Mutasi</div>
                            <?php
                                include_once "./backend/config/koneksi.php";

                                $querymutasi = mysqli_query($con,"SELECT * FROM mutasi");
                                $rowcountmutasi = mysqli_num_rows($querymutasi);
                            ?>
                            <div class="text number">
                                <?php echo $rowcountmutasi;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_pin</i>
                        </div>
                        <div class="content">
                            <div class="text">Pegawai</div>
                            <?php
                                include_once "./backend/config/koneksi.php";
                                $querypegawai = mysqli_query($con,"SELECT * FROM pegawai");
                                $rowcountpegawai = mysqli_num_rows($querypegawai);                                                            
                            ?>
                            <div class="text number">
                                <?php echo $rowcountpegawai; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2>Profil Desa</h2>
                    </div>
                    <div class="body">
                        <!-- <ol class="breadcrumb breadcrumb-bg-blue">
                            <li class="active"><i class="material-icons">info</i> Untuk melakukan perubahan data
                                silahkan klik data
                                informasi.</li>
                        </ol> -->
                        <div class="bs-example" data-example-id="media-alignment">
                            <div class="media">
                                <div class="media-left">
                                    <a href="javascript:void(0);">
                                        <img class="media-object" src="../../../images/logo/logo.jpeg" width="200"
                                            height="200">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for='inputNamaDesa'>Nama Desa</label>
                                            <div class="form-line">
                                                Wanajaya
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for='inputNamaDesa'>Nama Kecamatan</label>
                                            <div class="form-line">
                                                Tambakdahan
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for='inputNamaDesa'>Nama Kabupaten</label>
                                            <div class="form-line">
                                                Subang
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for='inputKodePos'>Kode Pos</label>
                                            <div class="form-line">
                                                41265
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputAlamat'>Alamat</label>
                                            <div class="form-line">
                                                Jl. Raya Wanajaya No.158, Ds.Wanajaya, Kec.Tambakdahan, Kab.Subang 41265 Jawa Barat
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label for='InputAlamat'>Gambaran Umum Desa</label>
                                                <div class="form-line">
                                                    <p><strong>Desa Wanajaya</strong> adalah desa hasil pemekaran dari Desa Mariuk menjadi 4 (empat) Desa, diantaranya adalah Desa Tanjungrasa, <strong>Desa Wanajaya</strong>,Desa Gardumukti dan Desa Mariuk sebagai induk,<strong>Desa Wanajaya</strong> dari 9 (sembilan) desa yang ada di wilayah Kecamatan Tambakdahan dan merupakan desa yang berbatasan dengan Kecamatan Sukasari serta merupakan dataran rendah dan mayoritas penduduknya bermata pencaharian petani.</p>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <label for='InputAlamat'>Visi Dan Misi Wanajaya</label>
                                                <div class="form-line">
                                                    <ul>
                                                        <li>Visi</li>
                                                        <p>"Terciptanya pemerintah desa yang sinergis dengan masyarakat demi mencapai kemajuan pembangunan desa yang adil, makmur dan sejahtera di segala bidang"</p>
                                                        <li>Misi</li>
                                                        <p>1. Meningkatkan sumber daya manusia yang berkualitas, berahlak mulia dan religuis</p>
                                                        <p>2. Perangkat Desa melaksanakan tugas pokok dan fungsi (tupoksi) secara profesional dan bertanggung-jawab</p>
                                                        <p>3. Menyelenggarakan Pemerintahan Desa secara terbuka serta mengutamakan musyawarah untuk mencapai mufakat</p>
                                                        <p>4. Menciptakan Pemerintahan Desa yang berpihak pada kepentingan masyarakat dan masyarakat yang aktif berpartisipasi mendukung program-program Desa</p>
                                                        <p>5. Terus melaksanakan pembangunan di segala bidang secara gotong royong adil dan merata.</p>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php include_once "./resources/src/_js.php"; ?>
</body>

</html>
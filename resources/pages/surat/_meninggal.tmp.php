<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
      </div>
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="header">
              <h2>Surat Keterangan Meninggal</h2>
              <button type="button" class="btn btn-primary waves-effect" id="buatSurat">Buat
                Surat</button>
            </div>
            <div class="body">
              <div id="formSuratMeninggal" style="display:none;">
                <form method="POST" autocomplete="off" id="form_add_surat_meninggal">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='inputNIK'>No Surat</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_surat" placeholder="No Surat" id="no_surat" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Meninggal</label>
                      <div class="form-line">
                        <input type="text" name="nik_meninggal" id="nik_meninggal" class="form-control" placeholder="NIK Meninggal">                        
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="toggleMeninggal" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td id="nama_meninggal"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="jk_meninggal"></td>
                        </tr>
                        <tr>
                          <td width="50">Alamat</td>
                          <td>:</td>
                          <td id="alamat_meninggal"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>Tanggal Meninggal</label>
                      <div class="form-line">
                        <input type="text" name="tgl_meninggal" id="tgl_meninggal" class="form-control datepicker"
                          placeholder="Tanggal Meninggal">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputAlamat'>Alamat Meninggal</label>
                      <div class="form-line">
                        <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_meninggal"
                          placeholder="Alamat Meninggal" id="alamat_meninggal"></textarea>
                      </div>
                    </div>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                  <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </form>
              </div>
              <div id="formEditSuratKelahiran" style="display:none;">
                <form method="POST" autocomplete="off" id="form_edit_surat_meninggal">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='inputNIK'>No Surat</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_surat_edit" placeholder="No Surat" id="no_surat_edit" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Meninggal</label>
                      <div class="form-line">
                        <input type="text" name="nik_meninggal_edit" id="nik_meninggal_edit" class="form-control"
                          placeholder="NIK Meninggal">
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="toggleMeninggal_edit" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td id="nama_meninggal_edit"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="jk_meninggal_edit"></td>
                        </tr>
                        <tr>
                          <td width="50">Alamat</td>
                          <td>:</td>
                          <td id="alamat_meninggal_edit"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>Tanggal Meninggal</label>
                      <div class="form-line">
                        <input type="text" name="tgl_meninggal_edit" id="tgl_meninggal_edit" class="form-control datepicker"
                          placeholder="Tanggal Meninggal">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputAlamat'>Alamat Meninggal</label>
                      <div class="form-line">
                        <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_meninggal_edit"
                          placeholder="Alamat Meninggal" id="alamat_meninggal_edit"></textarea>
                      </div>
                    </div>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Edit">
                  <button type="button" class="btn btn-danger waves-effect">CLOSE</button>
                </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table table-hover dashboard-task-infos" id="datatable_surat_meninggal">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>No Surat</th>
                    <th>Nama</th>
                    <th>Tanggal</th>
                    <th>Jenis Kelamin</th>
                    <th>Nama Admin</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="body bg-pink">
              <div class="font-bold m-b--35">Informasi</div>
              <ul class="dashboard-stat-list">
                <li>
                  Data harus valid. Silahkan lengkapi data disamping dengan data sebenarnya.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>
<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
      </div>
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="header">
              <h2>Daftar Surat Ket. Janda/Duda</h2>
              <button type="button" class="btn btn-primary waves-effect" id="buatSurat">Buat
                Surat</button>
            </div>
            <div class="body">
              <div id="formSuratCerai" style="display:none;">
                <form method="POST" autocomplete="off" id="form_add_surat_cerai">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='inputNIK'>No Surat</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_surat" placeholder="No Surat" id="no_surat" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Pemohon</label>
                      <div class="form-line">
                        <input type="text" name="nik_pemohon" id="nik_pemohon" class="form-control" placeholder="NIK Pemohon">                        
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="togglePemohon" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">NIK Pemohon</td>
                          <td>:</td>
                          <td id="nama_pemohon"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="jk_pemohon"></td>
                        </tr>
                        <tr>
                          <td width="50">Alamat</td>
                          <td>:</td>
                          <td id="alamat_pemohon"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Pasangan</label>
                      <div class="form-line">
                        <input type="text" name="nik_pasangan" id="nik_pasangan" class="form-control" placeholder="NIK Pasangan">          
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="togglePasangan" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">NIK Pasangan</td>
                          <td>:</td>
                          <td id="nama_pasangan"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="jk_pasangan"></td>
                        </tr>
                        <tr>
                          <td width="50">Alamat</td>
                          <td>:</td>
                          <td id="alamat_pasangan"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>Tanggal Cerai</label>
                      <div class="form-line">
                        <input type="text" name="tgl_cerai" id="tgl_cerai" class="form-control datepicker"
                          placeholder="Tanggal Cerai">
                      </div>
                    </div>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                  <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </form>
              </div>
            <div class="table-responsive">
              <table class="table table-hover dashboard-task-infos" id="datatable_surat_cerai">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>No Surat</th>
                    <th>NIK Pemohon</th>
                    <th>NIK Pasangan</th>
                    <th>Nama Admin</th>
                    <th>Tanggal Cerai</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>
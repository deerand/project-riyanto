<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
      </div>
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="header">
              <h2>Mutasi</h2>
              <button type="button" class="btn btn-primary waves-effect" id="buatMutasi">Buat Mutasi</button>
            </div>
            <div class="body">
              <div id="formMutasi" style="display:none;">
                <form method="POST" autocomplete="off" id="form_add_mutasi">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputNoRegister'>No Register</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_registrasi" placeholder="No Registrasi" id="no_registrasi" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputJK'>Jenis Mutasi</label>
                      <div class="form-line">
                        <select id="jenis_mutasi" class="form-control show-tick" name="jenis_mutasi">
                          <option value="">---Pilih---</option>
                          <option value="Masuk">Masuk</option>
                          <option value="Keluar">Keluar</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mutasiMasuk" style="display:none;">
                    <div class="form-group">
                      <label for='InputNamaLengkap'>Nama Lengkap</label>
                      <div class="form-line">
                        <input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control" placeholder="Nama Lengkap">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for='InputTglLahir'>Tanggal Lahir</label>
                      <div class="form-line">
                        <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datepicker" placeholder="Tanggal Lahir">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mutasiKeluar" style="display: none">
                    <div class="form-group">
                      <label for='InputCariNIK'>Cari NIK</label>
                      <div class="form-line">
                        <input type="text" name="cari_nik" id="cari_nik" class="form-control" placeholder="Cari NIK">
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="toggleDetailMutasi" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td id="detail_nama"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="detail_jk"></td>
                        </tr>
                        <tr>
                          <td width="50">Tempat Tinggal</td>
                          <td>:</td>
                          <td id="detail_tempat"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputJK'>Kategori Kepindahan</label>
                      <div class="form-line">
                        <select id="kategori_kepindahan" class="form-control show-tick" name="kategori_kepindahan">
                          <option value="">Pilih ketegori kepindahan</option>
                          <option value="Antar Desa/Kelurahan Dalam Satu Kecamatan">Antar Desa/Kelurahan Dalam Satu
                            Kecamatan</option>
                          <option value="Antar Desa/Kelurahan Dalam Satu Kabupaten/Kota">Antar Desa/Kelurahan Dalam
                            Satu Kabupaten/Kota</option>
                          <option value="Antar Desa/Kelurahan Dalam Satu Provinsi">Antar Desa/Kelurahan Dalam Satu
                            Provinsi</option>
                          <option value="Antar Kota/Kabupaten/Provinsi">Antar Kota/Kabupaten/Provinsi</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputAlamat'>Alamat Asal</label>
                      <div class="form-line">
                        <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_asal" placeholder="Alamat Asal"
                          id="alamat_asal"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputAlamat'>Alamat Pindah</label>
                      <div class="form-line">
                        <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_pindah" placeholder="Alamat Pindah"
                          id="alamat_pindah"></textarea>
                      </div>
                    </div>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                  <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
              </form>
            </div>
            <div class="table-responsive">
              <table class="table table-hover dashboard-task-infos" id="datatable_mutasi">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>No Surat</th>
                    <th>Jenis Mutasi</th>
                    <th>Kategori Kepindahan</th>
                    <th>NIK Pemohon</th>                    
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
      </div>
    </div>
    </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>
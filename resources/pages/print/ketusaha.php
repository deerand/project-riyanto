<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <?php include_once "../../../resources/src/_css.php"; ?>
  <style>
    .pull-right {
      float: right !important;
    }

    .separator {
      border-bottom: 2px solid #616161;
      margin: -0.5rem 0 1.5rem;
    }

    @media print {
      .separator {
        border-bottom: 2px solid #616161;
        margin: -0.5rem 0 1rem;
      }

    }
  </style>
</head>

<body onload="window.print()">
  <?php
    include_once "../../../backend/config/koneksi.php";
    $id = $_GET['id'];
    $query = mysqli_query($con,"SELECT
    penduduk.nama,
    penduduk.tmp_lahir,
    penduduk.tgl_lahir,
    penduduk.jk,
    penduduk.pekerjaan,
    penduduk.alamat,
    sketusaha.no_surat
  FROM
    sketusaha
    INNER JOIN penduduk
      ON sketusaha.nik_pemohon = penduduk.nik
  WHERE sketusaha.id = '$id'") or die(mysqli_error($con));
    while($data = mysqli_fetch_array($query))
    {
      $no_surat = $data['no_surat'];
      $nama = $data['nama'];
      $tmp_lahir = $data['tmp_lahir'];
      $tgl_lahir = $data['tgl_lahir'];
      $jk = $data['jk'];
      $pekerjaan = $data['pekerjaan'];
      $alamat = $data['alamat'];
    }
  ?>
  <div class="container">
    <div class="row">    
      <div class="col-md-12">
        <img src="../../../images/logo/logo.jpeg" alt="" width=100 height=100 style="float:left;margin-top=10px;">
        <h4 class="text-center">PEMERINTAH KABUPATEN SUBANG</h4>
        <h5 class="text-center">KECAMATAN TAMBAKDAHAN</h5>
        <h6 class="text-center">KELURAHAN WANAJAYA</h6>
        <p class="text-center">Jl. Raya Wanajaya No.158, Ds.Wanajaya, Kec.Tambakdahan, Kab.Subang 41265 Jawa Barat</p>
      </div>
    </div>
    <div class="separator"></div>
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:16px;"><strong><u>Surat Keterangan Usaha</u></strong></p>
        <p class="text-center" style="font-size:16px; margin-top: -15px">Nomer Surat: <?php echo $no_surat;?></p>
        <p class="text-left">Yang bertanda tangan di bawah ini Kepala Desa Wanajaya Kecamatan Tambahdahan Kabupaten Subang menerangkan dengan sebenar-benarnya bahwa</p>
        <div class="container">
          <table>
            <tbody>
              <tr>
                <td>1. </td>
                <td>Nama </td>
                <td>:
                  <?php echo $nama;?>
                </td>
              </tr>
              <tr>
                <td>2. </td>
                <td>Tempat Lahir </td>
                <td>:
                  <?php echo $tmp_lahir;?>
                </td>
              </tr>
              <tr>
                <td>3. </td>
                <td>Tanggal Lahir </td>
                <td>:
                  <?php echo $tgl_lahir;?>
                </td>
              </tr>
              <tr>
                <td>4. </td>
                <td>Jenis Kelamin </td>
                <td>:
                  <?php echo $jk;?>
                </td>
              </tr>
              <tr>
                <td>5. </td>
                <td>Pekerjaan </td>
                <td>:
                  <?php echo $pekerjaan;?>
                </td>
              </tr>
              <tr>
                <td>6. </td>
                <td>Alamat </td>
                <td>:
                  <?php echo $alamat;?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <p>Adalah benar yang bersangkutan warga Desa Wanajaya Kecamatan Tambakdahan Kabupaten Subang yang memiliki usaha.</p>
        <p>Demikian Surat Keterangan ini dibuat dan diberikan kepada yang bersangkutan untuk dapat</p>
        <div class="container">          
        <p>digunakan sebagaimana mestinya</p>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <table>
        <tr>
          <td>
            <p class="text-center">Kepala Desa</p>
          </td>
        </tr>
        <tr>
          <td rowspan="1">
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>

        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>

        <tr>
          <td>
            <p class="text-center">H. Sakim</p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-left" style="margin-top:-10px;">NIP.</p>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>
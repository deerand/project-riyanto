<?php session_start(); ?>


<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
        <h2>DASHBOARD</h2>
      </div>
      <!-- Widgets -->
      <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="header">
              <h2>Profil
                <?= $_SESSION['nama'];?>
              </h2>
            </div>
            <div class="body">
              <!-- <ol class="breadcrumb breadcrumb-bg-blue">
                            <li class="active"><i class="material-icons">info</i> Untuk melakukan perubahan data
                                silahkan klik data
                                informasi.</li>
                        </ol> -->
              <div class="bs-example" data-example-id="media-alignment">
                <div class="media">
                  <div class="media-body">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for='inputNip'>NIP Pegawai</label>
                        <div class="form-line">
                          <?= $_SESSION['nip'];?>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="form-group">
                        <label for='inputNamaPegawai'>Nama Pegawai</label>
                        <div class="form-line">
                          <?= $_SESSION['nama'];?>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='inputJabatan'>Jabatan</label>
                        <div class="form-line">
                          <?= $_SESSION['jabatan'];?>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='inputAlamat'>Alamat</label>
                        <div class="form-line">
                          <?= $_SESSION['alamat'];?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </section>
  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>
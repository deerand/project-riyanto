<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
    <?php include_once "../../../resources/partrials/_header.php"; ?>
    <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <?php include_once "../../../resources/partrials/_loader.php"; ?>
    <!-- #END# Page Loader -->
    <!-- Top Bar -->
    <?php include "../../../resources/partrials/_topbar.php"; ?>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
            <!-- #User Info -->
            <!-- Menu -->
            <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
            <!-- #Menu -->
            <!-- Footer -->
            <?php include_once "../../../resources/partrials/_footer.php"; ?>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
            </div>
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Pegawai Desa</h2>
                            <hr>
                            <button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_pegawai">Tambah
                                Pegawai</button>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos" id="datatable_data_pegawai">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Nama Lengkap</th>
                                            <th>NIP</th>
                                            <th>Jabatan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modal_add_pegawai" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="textHeaderModal">Tambah Pegawai</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" autocomplete="off" id="form_add_pegawai">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputNama'>Nama Lengkap</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap"
                                                    id="nama" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputAlamat'>Alamat</label>
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth" name="alamat"
                                                    placeholder="Alamat" id="alamat"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label for='InputJabatan'>Jabatan</label>
                                            <input type="text" class="form-control" name="jabatan" placeholder="Jabatan" id="jabatan" />
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for='InputNIP'>NIP</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nip" placeholder="NIP" id="nip" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputPassword'>Password</label>
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="password" placeholder="Password"
                                                    id="password" />
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modal_edit_pegawai" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="textHeaderModal">Edit Pegawai</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" autocomplete="off" id="form_edit_pegawai">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputNama'>Nama Lengkap</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama_edit" placeholder="Nama Lengkap"
                                                    id="nama_edit" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputAlamat'>Alamat</label>
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_edit"
                                                    placeholder="Alamat" id="alamat_edit"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label for='InputJabatan'>Jabatan</label>
                                            <input type="text" class="form-control" name="jabatan_edit" placeholder="Jabatan" id="jabatan_edit" />
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for='InputNIP'>NIP</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nip_edit" placeholder="NIP"
                                                    id="nip_edit" />
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modal_detail_pegawai" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="textHeaderModal">Detail Pegawai</h4>
                            </div>
                            <div class="modal-body">
                                <form method="POST" autocomplete="off" id="form_detail_pegawai">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for='inputNIK'>NIK</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nik_detail" placeholder="NIK"
                                                    id="nik_detail" />
                                                <input type="hidden" class="form-control" name="id_detail" id="id_detail">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputNama'>Nama Lengkap</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama_detail" placeholder="Nama Lengkap"
                                                    id="nama_detail" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label for='InputAlamat'>Alamat</label>
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_detail"
                                                    placeholder="Alamat" id="alamat_detail"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label for='InputJabatan'>Jabatan</label>
                                            <input type="text" class="form-control" name="jabatan_detail" placeholder="Jabatan" id="jabatan_detail" />
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for='InputNIP'>NIP</label>
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nip_detail" placeholder="NIP"
                                                    id="nip_detail" />
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="modal-footer">
                            
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-pink">
                            <div class="font-bold m-b--35">Informasi</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Data Desa harus valid. Silahkan lengkapi data disamping dengan data
                                    sebenarnya.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
    </section>

    <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>
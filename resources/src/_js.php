    <!-- Jquery Core Js -->
    <script src="http://localhost:8000/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="http://localhost:8000/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="http://localhost:8000/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="http://localhost:8000/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="http://localhost:8000/plugins/node-waves/waves.js"></script>

    <script src="http://localhost:8000/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="http://localhost:8000/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="http://localhost:8000/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    
    <!-- Autosize Plugin Js -->
    <script src="http://localhost:8000/plugins/autosize/autosize.js"></script>
    
    <!-- Moment Plugin Js -->
    <script src="http://localhost:8000/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="http://localhost:8000/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    
    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="http://localhost:8000/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

    <!-- ChartJs -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
    
    
    <!-- Custom Js -->
    <script src="http://localhost:8000/js/admin.js"></script>
    <script src='http://localhost:8000/js/pages/forms/basic-form-elements.js'></script>  
    <script src="http://localhost:8000/js/app.js"></script>    
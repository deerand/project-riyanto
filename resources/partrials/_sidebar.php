<div class="menu">
	<ul class="list">
		<li class="header">MAIN NAVIGATION</li>
		<li>
			<a href="javascript:void(0);">
				<i class="material-icons">home</i>
				<span>Home</span>
			</a>
		</li>
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">chrome_reader_mode</i>
				<span>Data Penunjang</span>
			</a>
			<ul class="ml-menu">
				<li>
					<a href="http://localhost:8000/resources/pages/pegawai/_dataPegawai.tmp.php">
						<span>Data Pegawai</span>
					</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">people</i>
				<span>Data Penduduk</span>
			</a>
			<ul class="ml-menu">
				<li>
					<a href="http://localhost:8000/resources/pages/penduduk/_dataPenduduk.tmp.php">
						<span>Semua</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/penduduk/_dataPendudukKurangMampu.tmp.php">
						<span>Kurang Mampu</span>
					</a>
				</li>
				<li>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/penduduk/_dataPendudukMeninggal.tmp.php">
						<span>Penduduk Meninggal</span>
					</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">email</i>
				<span>Layanan Surat</span>
			</a>
			<ul class="ml-menu">
				<li>
					<a href="http://localhost:8000/resources/pages/surat/_kelahiran.tmp.php">
						<span>Ket. Kelahiran</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/surat/_meninggal.tmp.php">
						<span>Ket. Meninggal</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/surat/_cerai.tmp.php">
						<span>Ket. Janda/Dunda</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/surat/_usaha.tmp.php">
						<span>Ket. Usaha</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/surat/_belumMenikah.tmp.php">
						<span>Ket. Belum Menikah</span>
					</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">send</i>
				<span>Mutasi Penduduk</span>
			</a>
			<ul class="ml-menu">
				<li>
					<a href="http://localhost:8000/resources/pages/mutasi/_dataMutasi.tmp.php">
						<span>Buat Mutasi</span>
					</a>
				</li>
			</ul>
		</li>
		<li>
			<a href="javascript:void(0);" class="menu-toggle">
				<i class="material-icons">graphic_eq</i>
				<span>Grafik Penduduk</span>
			</a>
			<ul class="ml-menu">
				<li>
					<a href="http://localhost:8000/resources/pages/grafik/_jkGraph.php">
						<span>Jenis Kelamin</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/grafik/_usiaGraph.php">
						<span>Usia</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/grafik/_agamaGraph.php">
						<span>Agama</span>
					</a>
				</li>
				<li>
					<a href="http://localhost:8000/resources/pages/grafik/_sKawinGraph.php">
						<span>Status Perkawinan</span>
					</a>
				</li>
			</ul>
		<li>
			<a href="http://localhost:8000/resources/pages/pegawai/_profilPegawai.php">
				<i class="material-icons">person</i>
				<span>Profil</span>
			</a>
		</li>
		</li>
	</ul>
</div>
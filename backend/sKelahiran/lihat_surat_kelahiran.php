<?php

include "../../backend/config/koneksi.php";

$requestData= $_REQUEST;
$columns = array( 
    // datatable column index  => database column name
	0 => 'no',	
    1 => 'no_surat',
    2 => 'nama',
    3 => 'nama_anak',
	4 => 'jk',
	5 => 'id',
);
// getting total number records without any search
$sql = "SELECT skelahiran.id, no_surat, nama, nama_anak, jk FROM skelahiran INNER JOIN pegawai ON skelahiran.nip_pegawai = pegawai.nip ORDER BY id";
$query=mysqli_query($con, $sql) or die(mysqli_error($con));
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT skelahiran.id, no_surat, nama, nama_anak, jk FROM skelahiran INNER JOIN pegawai ON skelahiran.nip_pegawai = pegawai.nip";
	$sql.=" WHERE no_surat LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR nama LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR nama_anak LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR jk LIKE '".$requestData['search']['value']."%' ";
	$sql.=" ORDER BY id ";
	
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.="".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.	
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
	} else {	
    $sql = "SELECT skelahiran.id, no_surat, nama, nama_anak, jk FROM skelahiran INNER JOIN pegawai ON skelahiran.nip_pegawai = pegawai.nip";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();
    
	$nestedData['no'] = $no++;
	$nestedData['id'] = $row["id"];
	$nestedData['no_surat'] = $row["no_surat"];
	$nestedData['nama'] = $row["nama"];
	$nestedData['nama_anak'] = $row["nama_anak"];
	$nestedData['jk'] = $row["jk"];
	
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>
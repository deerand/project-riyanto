<?php

include "../../backend/config/koneksi.php";

$requestData= $_REQUEST;
$columns = array( 
    // datatable column index  => database column name
  0 => 'no',
  1 => 'no_surat',
  2 => 'nama',
  3 => 'nik_pemohon',
  4 => 'nik_pasangan',
	5 => 'tgl_cerai',
	6 => 'id'
);
// getting total number records without any search
$sql = "SELECT scerai.no_surat, pegawai.nama, nik_pemohon, nik_pasangan, tgl_cerai FROM scerai INNER JOIN pegawai ON scerai.nip_pegawai=pegawai.nip ORDER BY scerai.id";
$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT scerai.no_surat, pegawai.nama, nik_pemohon, nik_pasangan, tgl_cerai FROM scerai INNER JOIN pegawai ON scerai.nip_pegawai=pegawai.nip";
	$sql.=" WHERE no_surat LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
	$sql.=" OR nama LIKE '".$requestData['search']['value']."%' ";
  $sql.=" OR nik_pemohon LIKE '".$requestData['search']['value']."%' ";
  $sql.=" OR nik_pasangan LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR tgl_cerai LIKE '".$requestData['search']['value']."%' ";
	$sql.=" ORDER BY scerai.id ";
	
	
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.="".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees"); // again run query with limit
	
	} else {	
    $sql = "SELECT scerai.no_surat, pegawai.nama, nik_pemohon, nik_pasangan, tgl_cerai FROM scerai INNER JOIN pegawai ON scerai.nip_pegawai=pegawai.nip";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();
    
	$nestedData['no'] = $no++;
	$nestedData['no_surat'] = $row["no_surat"];
	$nestedData['nama'] = $row["nama"];
	$nestedData['nik_pemohon'] = $row["nik_pemohon"];
  $nestedData['nik_pasangan'] = $row["nik_pasangan"];
  $nestedData['tgl_cerai'] = $row["tgl_cerai"];
	
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>
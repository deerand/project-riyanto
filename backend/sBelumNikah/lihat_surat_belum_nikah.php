<?php

include "../../backend/config/koneksi.php";

$requestData= $_REQUEST;
$columns = array( 
  // datatable column index  => database column name
	0 => 'no',
  1 => 'no_surat',
  2 => 'nama',
  3 => 'jk',
  5 => 'sperkawinan',
  6 => 'nama_pegawai',
  7 => 'created_at',
  8 => 'id',
);
// getting total number records without any search
$sql = "SELECT sblmnikah.id,sblmnikah.no_surat,penduduk.nama,penduduk.jk,penduduk.sperkawinan,pegawai.nama AS nama_pegawai,sblmnikah.created_at FROM sblmnikah INNER JOIN pegawai ON sblmnikah.nip_pegawai=pegawai.nip INNER JOIN penduduk ON sblmnikah.nik_pemohon=penduduk.nik ORDER BY sblmnikah.id";
$query=mysqli_query($con, $sql) or die(mysqli_error($con));
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT sblmnikah.id,sblmnikah.no_surat,penduduk.nama,penduduk.jk,penduduk.sperkawinan,pegawai.nama AS nama_pegawai,sblmnikah.created_at FROM sblmnikah INNER JOIN pegawai ON sblmnikah.nip_pegawai=pegawai.nip INNER JOIN penduduk ON sblmnikah.nik_pemohon=penduduk.nik ORDER BY sblmnikah.id";
	$sql.=" WHERE no_surat LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
	$sql.=" OR nama LIKE '".$requestData['search']['value']."%' ";	
	$sql.=" OR jk LIKE '".$requestData['search']['value']."%' ";
  $sql.=" OR nama_pegawai LIKE '".$requestData['search']['value']."%' ";
  $sql.=" OR created_at LIKE '".$requestData['search']['value']."%' ";
	$sql.=" ORDER BY sblmnikah.id ";
	
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.="".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
	} else {	
  $sql = "SELECT sblmnikah.id,sblmnikah.no_surat,penduduk.nama,penduduk.jk,penduduk.sperkawinan,pegawai.nama AS nama_pegawai,sblmnikah.created_at FROM sblmnikah INNER JOIN pegawai ON sblmnikah.nip_pegawai=pegawai.nip INNER JOIN penduduk ON sblmnikah.nik_pemohon=penduduk.nik";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();
    
  $nestedData['no'] = $no++;
  $nestedData['id'] = $row["id"];
	$nestedData['no_surat'] = $row["no_surat"];
	$nestedData['nama'] = $row["nama"];	
  $nestedData['jk'] = $row["jk"];
  $nestedData['sperkawinan'] = $row["sperkawinan"];
  $nestedData['nama_pegawai'] = $row["nama_pegawai"];
  $nestedData['created_at'] = $row["created_at"];
	
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>
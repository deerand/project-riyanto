<?php

include "../../backend/config/koneksi.php";

$requestData= $_REQUEST;
$columns = array( 
    // datatable column index  => database column name
  0 => 'no',
  1 => 'no_surat',
	2 => 'jenis_mutasi',
	3 => 'kategori_kepindahan',
	4 => 'nik_pemohon'
);
// getting total number records without any search
$sql = "SELECT * FROM mutasi ORDER BY id";
$query=mysqli_query($con, $sql) or die(mysqli_error($con));
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT * FROM mutasi ORDER BY id";
  $sql.=" WHERE no_surat LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
  $sql.=" OR jenis_mutasi LIKE '".$requestData['search']['value']."%' ";  
	
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.=" ORDER BYda ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($con, $sql) or die(mysqli_error($con)); // again run query with limit
	
	} else {	
		$sql = "SELECT * FROM mutasi";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();
    
	$nestedData['no'] = $no++;
	$nestedData['no_surat'] = $row["no_surat"];
	$nestedData['jenis_mutasi'] = $row["jenis_mutasi"];
	$nestedData['kategori_kepindahan'] = $row["kategori_kepindahan"];
  $nestedData['nik_pemohon'] = $row["nik_pemohon"];  
	
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>
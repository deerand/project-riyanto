<?php
include "../../backend/config/koneksi.php";
$query = "SELECT 
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 0 AND 4,1, NULL)) '0-4',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 5 AND 9,1, NULL)) '5-9',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 10 AND 14,1, NULL)) '10-14',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 15 AND 19,1, NULL)) '15-19',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 20 AND 24,1, NULL)) '20-24',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 25 AND 29,1, NULL)) '25-29',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 30 AND 34,1, NULL)) '30-34',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 35 AND 39,1, NULL)) '35-39',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 40 AND 44,1, NULL)) '40-44',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 45 AND 49,1, NULL)) '45-49',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 50 AND 54,1, NULL)) '50-54',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) BETWEEN 55 AND 59,1, NULL)) '55-59',
COUNT(IF(TIMESTAMPDIFF( YEAR, tgl_lahir, CURDATE( ) ) >=60 ,1, NULL)) '60'
FROM penduduk";
$sql = mysqli_query($con,$query);
while($data = mysqli_fetch_array($sql))
{    
    $json_data['0-4'] = $data['0-4'];
    $json_data['5-9'] = $data['5-9'];
    $json_data['10-14'] = $data['10-14'];
    $json_data['15-19'] = $data['15-19'];
    $json_data['20-24'] = $data['20-24'];
    $json_data['25-29'] = $data['25-29'];
    $json_data['30-34'] = $data['30-34'];
    $json_data['35-39'] = $data['35-39'];
    $json_data['40-44'] = $data['40-44'];
    $json_data['45-49'] = $data['45-49'];
    $json_data['50-54'] = $data['50-54'];
    $json_data['55-59'] = $data['55-59'];
    $json_data['60+'] = $data['60'];
    
}
$json_array = $json_data;
echo json_encode($json_array);

?>










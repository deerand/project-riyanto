<?php
include "../../backend/config/koneksi.php";
$query = "SELECT COUNT(IF(jk='P',1, NULL)) 'Pria', COUNT(IF(jk='W',1, NULL)) 'Wanita' FROM penduduk";
$sql = mysqli_query($con,$query);
$json_data = array();
while($data = mysqli_fetch_array($sql))
{    
    $json_data['pria'] = $data['Pria'];
    $json_data['wanita'] = $data['Wanita'];
}
$json_array = $json_data;
echo json_encode($json_array);
<?php

include "../../backend/config/koneksi.php";
$query = "SELECT 
            COUNT(IF(sPerkawinan='Kawin',1, NULL)) 'Kawin',
            COUNT(IF(sPerkawinan='Belum Kawin',1, NULL)) 'Belum Kawin',
            COUNT(IF(sPerkawinan='Cerai',1, NULL)) 'Cerai'            
          FROM penduduk";
$sql = mysqli_query($con,$query);
while($data = mysqli_fetch_array($sql))
{    
    $json_data['Kawin'] = $data['Kawin'];
    $json_data['Belum Kawin'] = $data['Belum Kawin'];
    $json_data['Cerai'] = $data['Cerai'];    
}
$json_array = $json_data;
echo json_encode($json_array);